

abstract class Device
{
	public static final int STATE_ERROR = -1;
	public static final int STATE_STANDBY = 0;
	public static final int STATE_PREPARE = 1;
	public static final int STATE_READY = 2;
	
	protected volatile int state;
	protected volatile int data;
	
	public int getState()
	{ return state; }
	
	public void setState(int state)
	{ this.state = state; }
	
	public void putData(int data)
	{ this.data = data; }
	
	public int getData()
	{ return data; }

	public abstract void sendCommand(int cmd);
}

class KeyBoard extends Device implements Runnable
{
	static final String LINESEP;
	static final char LINEEND;
	static {
		LINESEP = java.lang.System.lineSeparator();
		LINEEND = LINESEP.charAt(LINESEP.length() - 1);
	}
	
	final java.lang.Object lock = new java.lang.Object();
	volatile java.lang.Thread thread;
	
	public KeyBoard() {
		state = STATE_STANDBY;
		thread = null;
	}
	
	@Override public void sendCommand(int cmd) {
		synchronized (lock) {
			if (thread != null) {
				return;
			}
			if (cmd == 1) {
				thread = new java.lang.Thread(this);
				thread.start();
			} else {
				state = STATE_ERROR;
			}
		}
	}
	
	@Override public void run()
	{
		try {
			for(;;) {
				if (state == STATE_PREPARE) {
					data = System.in.read();
					if (data < 0 || data == LINEEND) {
						break;
					} else if (LINESEP.indexOf(data) < 0) {
						state = STATE_READY;
					}
				}
				java.lang.Thread.sleep(1);
			}
			state = STATE_STANDBY;
		} catch (java.lang.Exception ex) {
			state = STATE_ERROR;
		}
		thread = null;
	}
}

class Display extends Device
{
	public void sendCommand(int cmd) {
	}

}


class Test
{
	static void input(Device dev) throws java.lang.Exception
	{
		System.out.println("INPUT");
		
		dev.sendCommand(1);
		dev.setState(Device.STATE_PREPARE);
		int state;
		int buf;
		for (;;) {
			while ((state = dev.getState()) == Device.STATE_PREPARE) {
				Thread.sleep(1);
			}
			if (state == Device.STATE_READY) {
				buf = dev.getData();
				dev.setState(Device.STATE_PREPARE);
				System.out.println("buf: " + (char)buf);
			} else if (state == Device.STATE_STANDBY) {
				System.out.println("all done");
				break;
			} else if (state == Device.STATE_ERROR) {
				System.out.println("error");
				break;
			}
		}
	}
	
	public static void main(String[] args) throws java.lang.Exception
	{
		Device dev = new KeyBoard();
		
		input(dev);
		
		input(dev);
	}
	
}